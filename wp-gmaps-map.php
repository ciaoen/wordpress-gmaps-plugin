<?php
wp_enqueue_style('wpstyle');

global $wpdb;

$map_data = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "map" );

$latitude = $map_data->latitude;
$longitude= $map_data->longitude;

$markers = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "marker");
?>
<script>
jQuery(document).ready(function(){
    var map;
    
    map = new GMaps({
        div: '#map',                
        lat: <?php echo $latitude; ?>,
        lng: <?php echo $longitude; ?>
    });
    
    <?php foreach ($markers as $marker):?>
    map.addMarker({
        lat: <?php echo $marker->latitude; ?>,
        lng: <?php echo $marker->longitude; ?>,
        title: <?php echo "'" . $marker->description . "'"; ?>
    });
    <?php endforeach; ?>
});
</script>
<div id="map"></div>