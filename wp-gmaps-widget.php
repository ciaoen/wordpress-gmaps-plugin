<?php
class Wp_gmaps_widget extends WP_Widget {
    public function __construct() {
        parent::__construct(
            'Wp_gmaps_widget',
            __('WP-GMaps Address Info', PLUGIN_NAME),
            array('description' => __('Displays the address info of the specified map center.', PLUGIN_NAME),)
            );
    }
    
    public function widget($args, $instance) {
        global $wpdb;
        
        $address_data = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "map" );
        
        echo "<aside class='widget'>";
        
        if ($address_data != NULL) {
            echo "<p>" . $address_data->address . "<br>" . $address_data->zip . " " 
                . $address_data->city . "</p>";
        }
        
        else {
            echo "<p>" . __('Map Center data has not been set!', PLUGIN_NAME) . "</p>";
        }
        
        echo "</aside>";
    }
    
    public function form($instance) {
        //There are no settings for the administrator.
    }
    
    public function update($new_instance, $old_instance) {
        //No information needs to be saved.
    }
}