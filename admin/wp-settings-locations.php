<?php
if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class Wp_gmaps_locations_table extends WP_List_Table {

    /** Class constructor */
    public function __construct() {
        parent::__construct( [
            'singular' => __( 'Location', PLUGIN_NAME ), //singular name of the listed records
            'plural'   => __( 'Locations', PLUGIN_NAME ), //plural name of the listed records
            'ajax'     => false //does this table support ajax?
        ] );
    }

    public static function get_locations( $per_page = 10, $page_number = 1 ) {

        global $wpdb;

        $sql = "SELECT * FROM {$wpdb->prefix}marker";

        if (!empty( $_REQUEST['orderby'] ) ) {
            $sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
            $sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
        }

        $sql .= " LIMIT $per_page";
        $sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;

        $result = $wpdb->get_results( $sql, 'ARRAY_A' );

        return $result;
    }

    public static function delete_location( $id ) {
        global $wpdb;

        $wpdb->delete(
            "{$wpdb->prefix}marker",
            [ 'id' => $id ],
            [ '%d' ]
        );
    }

    public static function record_count() {
        global $wpdb;

        $sql = "SELECT COUNT(*) FROM {$wpdb->prefix}marker";

        return $wpdb->get_var( $sql );
    }

    public function no_items() {
        _e( 'No locations saved.', PLUGIN_NAME );
    }

    public function column_default( $item, $column_name ) {
        switch ( $column_name ) {
            case 'description':
            case 'latitude':
            case 'longitude':
                return $item[ $column_name ];
            default:
                return print_r( $item, true ); //Show the whole array for troubleshooting purposes
        }
    }

    function column_cb( $item ) {
        return sprintf(
            '<input type="checkbox" name="bulk-delete[]" value="%s" />', $item['id']
        );
    }

    function column_name( $item ) {

        $delete_nonce = wp_create_nonce( 'sp_delete_location' );

        $title = '<strong>' . $item['name'] . '</strong>';

        $actions = [
            'delete' => sprintf( '<a href="?page=%s&action=%s&location=%s&_wpnonce=%s">Delete</a>', esc_attr( $_REQUEST['page'] ), 'delete', absint( $item['id'] ), $delete_nonce )
        ];

        return $title . $this->row_actions( $actions );
    }

    function get_columns() {
        $columns = [
            'cb'            => '<input type="checkbox" />',
            'description'   => __( 'Description', PLUGIN_NAME ),
            'latitude'      => __( 'Latitude', PLUGIN_NAME ),
            'longitude'     => __( 'Longitude', PLUGIN_NAME )
        ];

        return $columns;
    }

    public function get_sortable_columns() {
        $sortable_columns = array(
            'description' => array( 'description', true ),
            'latitude'    => array( 'latitude', false ),
            'longitude'   => array( 'longitude', false )
        );

        return $sortable_columns;
    }
    
    public function get_bulk_actions() {
        $actions = [
            'bulk-delete' => 'Delete'
        ];

        return $actions;
    }

    public function prepare_items() {

        //$this->_column_headers = $this->get_column_info();
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array($columns, $hidden, $sortable);

        $this->process_bulk_action();

        $per_page     = 10; //$this->get_items_per_page( 'locations_per_page', 10 );
        $current_page = $this->get_pagenum();
        $total_items  = self::record_count();

        $this->set_pagination_args( [
            'total_items' => $total_items, //WE have to calculate the total number of items
            'per_page'    => $per_page //WE have to determine how many items to show on a page
        ] );

        $this->items = self::get_locations( $per_page, $current_page );
    }

    public function process_bulk_action() {

        //Detect when a bulk action is being triggered...
        if ( 'delete' === $this->current_action() ) {

            // In our file that handles the request, verify the nonce.
            $nonce = esc_attr( $_REQUEST['_wpnonce'] );

            if ( ! wp_verify_nonce( $nonce, 'sp_delete_location' ) ) {
                die( 'Go get a life script kiddies' );
            }
            else {
                self::delete_location( absint( $_GET['location'] ) );

//                wp_redirect( esc_url( add_query_arg() ) );
//                exit;
            }
        }

        // If the delete bulk action is triggered
        if ( ( isset( $_POST['action'] ) && $_POST['action'] == 'bulk-delete' )
             || ( isset( $_POST['action2'] ) && $_POST['action2'] == 'bulk-delete' )
        ) {
            
            if (isset($_POST['bulk-delete'])) {
                $delete_ids = esc_sql( $_POST['bulk-delete'] );

                // loop over the array of record IDs and delete them

                foreach ( $delete_ids as $id ) {
                    self::delete_location( $id );
                }
                
                echo "<div class='updated'><p>";
                _e('Selected locations deleted.', PLUGIN_NAME);
                echo "</p></div>";
            }
            
//            wp_redirect( esc_url( add_query_arg() ) );
//            exit;
        }
    }
}

wp_enqueue_style('wpstyle');

$locations_table = new Wp_gmaps_locations_table();

?>
<div class="wrap">
    <h2><?php _e('Locations', PLUGIN_NAME);?></h2>
    <div id="poststuff">
        <div id="post-body" class="metabox-holder columns-2">
            <div id="post-body-content">
                <div class="meta-box-sortables ui-sortable">
                    <form method="post">
                        <?php
                        $locations_table->prepare_items();
                        $locations_table->display(); 
                        ?>
                    </form>
                </div>
            </div>
        </div>
        <br class="clear">
    </div>
</div>