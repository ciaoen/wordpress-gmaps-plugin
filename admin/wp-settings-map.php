<?php
wp_enqueue_style('wpstyle');
?>
<div class="wrap">
    <?php
    global $wpdb;
    $latitude="";
    $longitude="";
    $address="";
    $zip="";
    $city="";
    $table_name = $wpdb->prefix . "map";
    
    if (isset($_POST["latitude"]) && isset($_POST["longitude"])) {
    ?>
    <div class="updated"><p>
    <?php
        $latitude = sanitize_text_field($_POST["latitude"]);
        $longitude = sanitize_text_field($_POST["longitude"]);
        $address = sanitize_text_field($_POST["address"]);
        $zip = sanitize_text_field($_POST["zip"]);
        $city = sanitize_text_field($_POST["city"]);

        $wpdb->replace (
            $table_name,
            array (
                'id' => 1,
                'latitude' => $latitude,
                'longitude' => $longitude,
                'address' => $address,
                'zip' => $zip,
                'city' => $city
            ),
            array (
                '%d',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s'
            )
        );

        _e('Map saved.', PLUGIN_NAME)
    ?>
    <p></div>
    <?php }
    else {
        $map = $wpdb->get_row("SELECT * FROM " . $table_name);
        if ($map != null) {
            $latitude = $map->latitude;
            $longitude = $map->longitude;
            $address = $map->address;
            $zip = $map->zip;
            $city = $map->city;
        }
    }?>
    <h2><?php _e('Set Map Center', PLUGIN_NAME);?></h2>
    <form method="post" action="">
        <table class="form-table">
            <tbody>
                <tr valign="top">
                    <th scope="row">
                        <label for="latitude"><?php _e('Latitude', PLUGIN_NAME);?>:</label>
                    </th>
                    <td>
                        <input id="latitude" name="latitude" size="9" maxlength="9"
                               value="<?php print($latitude);?>" required>                     
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <label for="longitude"><?php _e('Longitude', PLUGIN_NAME);?>:</label>
                    </th>
                    <td>
                        <input id="longitude" name="longitude" size="9" maxlength="9"
                               value="<?php print($longitude);?>" required>                     
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <label for="address"><?php _e('Address', PLUGIN_NAME);?>:</label>
                    </th>
                    <td>
                        <input id="address" name="address" size="50" maxlength="50"
                               value="<?php print($address);?>">                     
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <label for="zip"><?php _e('Zip Code', PLUGIN_NAME);?>:</label>
                    </th>
                    <td>
                        <input id="zip" name="zip" size="15" maxlength="15"
                               value="<?php print($zip);?>">                     
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <label for="city"><?php _e('City', PLUGIN_NAME);?>:</label>
                    </th>
                    <td>
                        <input id="city" name="city" size="50" maxlength="50"
                               value="<?php print($city);?>">                     
                    </td>
                </tr>
            </tbody>
        </table>
        <input type="submit" class="button button-primary" value="<?php _e('Save', PLUGIN_NAME) ?>">
    </form>
</div>