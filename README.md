# WordPress GMaps Plugin #

### FI ###

PHP:llä ja JavaScriptillä ohjelmoitu WordPress-lisäosa, joka hyödyntää Googlen GMaps-kirjastoa. Lisäosan avulla sivulle voidaan generoida lyhytkoodilla **[Map]** kartta, jonka keskipisteen ja nastat voi määrittää uudelleen. Sisältää myös yksinkertaisen vimpaimen, joka näyttää kartan keskipisteen osoitetiedot.

Lisäosaa voi testata testikäyttäjällä osoitteessa https://n4maju01.studyingroom.net/wpgmaps/

* **Käyttäjätunnus**: tester
* **Salasana**: tester

Itse lisäosa on ladattavissa osoitteessa https://n4maju01.studyingroom.net/dl/wp-gmaps.zip

### EN ###

WordPress plugin written in PHP and JS utilizing Google's GMaps library. The plugin allows you to use the shortcode **[Map]** to generate a map with customizable center and markers on your page. Also includes a simple widget which displays current map center address info.

You can test the plugin with the test user at https://n4maju01.studyingroom.net/wpgmaps/

* **Username**: tester
* **Password**: tester

The plugin itself can be downloaded from https://n4maju01.studyingroom.net/dl/wp-gmaps.zip